
## 0.0.7 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/velocloud-sd-wan!16

---

## 0.0.6 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/velocloud-sd-wan!15

---

## 0.0.5 [01-17-2022]

* 2021.2 certification

See merge request itentialopensource/pre-built-automations/velocloud-sd-wan!13

---

## 0.0.4 [10-20-2021]

* certify 2021.1

See merge request itentialopensource/pre-built-automations/velocloud-sd-wan!12

---

## 0.0.3 [10-20-2021]

* certify 2020.2

See merge request itentialopensource/pre-built-automations/velocloud-sd-wan!11

---

## 0.0.2 [10-19-2021]

* Delete .gitlab-ci.yml

See merge request itentialopensource/pre-built-automations/velocloud-sd-wan!9

---\n\n
