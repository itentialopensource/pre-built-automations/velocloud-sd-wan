<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your artifact name -->
# VeloCloud SD-WAN Management

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Features](#features)
* [Requirements](#requirements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview

<!-- Write a few sentences about the artifact and explain the use case(s) -->
The VeloCloud SD-WAN artifact creates VeloCloud Enterprises and provisions a list of VeloCloud Edges at corresponding locations.

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<table><tr><td>
  <img src="images/workflow.png" alt="workflow" width="800px">
</td></tr></table>

<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

## Features

<!-- Unordered list highlighting the most exciting features of the artifact -->
 * Validates that the Enterprise name is unique.
 * Creates the Enterprise Network
 * Supports provisioning of multiple edges 
 * Displays edge activation key to the user

## Requirements

In order to use this artifact, users will have to satisfy the following pre-requisites:

* Itential Automation Platform
* app-artifacts
* adapter-velocloudOrchestrator


## How to Install

Please ensure that you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the artifact. If you do not currently have App-Artifacts installed on your server, please download the installer from your Nexus repository. Please refer to the instructions included in the App-Artifacts README to install it.

The artifact can be installed from within App-Artifacts. Simply search for the name of your desired artifact and click the install button as shown below:

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

<table><tr><td>
<img  src="./images/install.gif"  alt="install"  width="1000px">
</td></tr></table>
<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED ARTIFACT -->

Alternatively, you may clone this repository and run `npm pack` to create a tarball which can then be installed via the offline installer in App-Artifacts. Please consult the documentation for App-Artifacts for further information.

VeloCloud API docs: https://code.vmware.com/apis/556/velocloud-sdwan-vco-api 

## How to Run

Go to Automation Studio and run "SD-WAN using Velocloud Orchestration"
It comes with pre-fill form data.
<table><tr><td>
  <img src="images/vco-order-form.png" alt="workflow" width="300px">
</td></tr></table>
<table><tr><td>
  <img src="images/form-2.png" alt="workflow" width="600px">
</td></tr></table>
<table><tr><td>
  <img src="images/edge-keys.png" alt="workflow" width="300px">
</td></tr></table>
<table><tr><td>
  <img src="images/vco-edges.png" alt="workflow" width="300px">
</td></tr></table>
